function parseInputToMap(selectedItems) {
  const parsedInputItems = new Map();
  const separatedItems = selectedItems.split(',');
  separatedItems.forEach(item => {
    const itemRow = item.split(' ');
    parsedInputItems.set(itemRow[0], parseInt(itemRow.pop()));
  });
  return parsedInputItems;
}

export { parseInputToMap };
