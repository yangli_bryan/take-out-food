import dishes from './menu.js';

class Order {
  constructor(itemsMap) {
    this.itemsMap = itemsMap;
  }

  get itemDetails() {
    const itemDetails = [];
    for (const [key, value] of this.itemsMap.entries()) {
      const item = dishes.find(menuItem => menuItem.id === key);
      item.count = value;
      itemDetails.push(item);
    }
    return itemDetails;
  }

  get totalPrice() {
    return this.itemDetails.reduce((accumulator, item) => accumulator + item.count * item.price, 0);
  }
}

export default Order;
